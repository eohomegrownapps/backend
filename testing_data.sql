-- RACTF Backend
-- Copyright (C) 2019  RACTF Contributors
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

BEGIN;

-- clear everything first
DELETE FROM hints;
DELETE FROM challenges;

insert into challenge_groups
(id, display_order, name, contained_type) values
('0cb2d081-5435-4dbf-a444-73775da76f21', 1, 'testing group', 'campaign')
;

insert into challenges
(id, challenge_group, type, name, base_score, flag_type, flag_info, metadata) values
('ef660def-cc9e-4df5-ba6f-38b193bf5840', '0cb2d081-5435-4dbf-a444-73775da76f21',
'', 'testing challenge 1', 200, 'basic', '{"flag":"abcd1234"}'::jsonb, '{"x":0, "y":0}'::jsonb),
('8ea76ec5-bc1f-4d69-860a-8f1d34459c3a', '0cb2d081-5435-4dbf-a444-73775da76f21',
'', 'testing challenge 2', 200, 'hashed', '{"flag_hash":"e9cee71ab932fde863338d08be4de9dfe39ea049bdafb342ce659ec5450b69ae"}'::jsonb, '{"x":1,"y":1}'::jsonb)
;




COMMIT;
