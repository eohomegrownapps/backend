TABLES = {
    "teams": [
        "id              UUID PRIMARY KEY",
        "name            TEXT NOT NULL",
        "upper_name      TEXT NOT NULL",
        "visible         BOOLEAN DEFAULT true",
        "password        TEXT NOT NULL",
        "owner           UUID NOT NULL",
        "description     TEXT",
    ],
    "users": [
        "id              UUID PRIMARY KEY",
        "name            TEXT NOT NULL",
        "upper_name      TEXT NOT NULL",
        "email           TEXT NOT NULL",
        "enabled         BOOLEAN DEFAULT TRUE",
        "visible         BOOLEAN DEFAULT TRUE",
        "joined          TIMESTAMP",
        "totp_secret     TEXT",
        "totp_status     TEXT NOT NULL DEFAULT 'off'",
        "hashed_password TEXT NOT NULL",
        "admin           BOOLEAN NOT NULL DEFAULT FALSE",
        "email_verified  BOOLEAN NOT NULL DEFAULT FALSE",
        "team            UUID REFERENCES teams",
        "reset_secret    TEXT",
        "reset_timestamp TIMESTAMP",
        "bio             TEXT",
        "discord         TEXT",
        "discordid       TEXT",
        "twitter         TEXT",
        "reddit          TEXT",
    ],
    "challenge_groups": [
        "id              UUID PRIMARY KEY",
        "display_order   INT NOT NULL",
        "name            TEXT NOT NULL",
        "contained_type  TEXT NOT NULL",
        "description     TEXT",
        "metadata        JSONB",
    ],
    "challenges": [
        "id              UUID PRIMARY KEY",
        "challenge_group UUID NOT NULL REFERENCES challenge_groups",
        "type            TEXT",
        "metadata        JSONB",
        "name            TEXT NOT NULL",
        "description     TEXT",
        "author          TEXT",
        "base_score      INT NOT NULL",
        "flag_type       TEXT NOT NULL",
        "flag_info       JSONB",
        "auto_unlock     BOOLEAN NOT NULL DEFAULT FALSE",
    ],
    "challenge_links": [
        "required        UUID NOT NULL REFERENCES challenges",
        "unlocks         UUID NOT NULL REFERENCES challenges",
        ["(required, unlocks)"]
    ],
    "files": [
        "id              UUID PRIMARY KEY",
        "name            TEXT NOT NULL",
        "url             TEXT NOT NULL",
        "size            NUMERIC NOT NULL DEFAULT 0",
        "challenge       UUID NOT NULL REFERENCES challenges",
    ],
    "hints": [
        "id              UUID PRIMARY KEY",
        "challenge_id    UUID NOT NULL REFERENCES challenges",
        "message         TEXT NOT NULL",
        "penalty         INT NOT NULL",
        "name            TEXT NOT NULL",
    ],
    "categories": [
        "id              UUID PRIMARY KEY",
        "name            TEXT NOT NULL",
        "display_order   INT",
    ],
    "hints_used": [
        "user_id         UUID NOT NULL REFERENCES users",
        "hint            UUID NOT NULL REFERENCES hints",
        ["(user_id, hint)"],
    ],
    "solves": [
        "user_id         UUID NOT NULL REFERENCES users",
        "challenge       UUID NOT NULL REFERENCES challenges",
        "solve_timestamp TIMESTAMP NOT NULL",
    ],
    "config": [
        "key             TEXT NOT NULL PRIMARY KEY",
        "value           JSONB",
    ]
}


def prepare():
    sql = []
    for i in TABLES:
        sql.append(f"CREATE TABLE IF NOT EXISTS {i}();")
        for j in TABLES[i]:
            if not isinstance(j, list):
                sql.append(
                    f"""DO $$
        BEGIN
            BEGIN
                ALTER TABLE {i}
                ADD COLUMN {j};
            EXCEPTION
                WHEN duplicate_column THEN NULL;
            END;
        END;
    $$;
    """
                )
            else:
                sql.append(
                    f"""DO $$
        BEGIN
            ALTER TABLE {i} ADD PRIMARY KEY {j[0]};
        END;
    $$;
    """
                )

    return sql
