# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import importlib

import asyncpg

from starlette.applications import Starlette
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import PlainTextResponse

from .config import config
from .db import get_pool

# I anticipate using a mixture of sub-apps (like this one), and Endpoints
# (class-based routes)

modules = (
    # UPDATE THIS AS YOU ADD MORE MODULES
    "test",
    "countdown",
    "auth",
    "members",
    "challenges",
    "teams",
    "leaderboard",
    "group",
    "admin",
    "files",
    "hints",
)

debug = config.getboolean("dev", "debug")

app = Starlette(debug=debug)


@app.on_event("startup")
async def on_startup():
    from . import schema

    pool = await get_pool()
    app.state.db_pool = pool
    for sub_app in app.state.sub_apps:
        sub_app.state.db_pool = pool
    pool = app.state.db_pool

    async with pool.acquire() as conn:
        schema_sql = schema.prepare()
        for i in schema_sql:
            try:
                await conn.execute(i)
            except asyncpg.exceptions.InvalidTableDefinitionError as e:
                print("DB setup error:", e)

        config = await conn.fetch("SELECT key, value FROM config")
        config = {i["key"]: i["value"] for i in config}

        if "login" not in config:
            await conn.execute("INSERT INTO config (key, value) VALUES ($1, $2)", "login", "true")
        if "registration" not in config:
            await conn.execute("INSERT INTO config (key, value) VALUES ($1, $2)", "registration", "true")
        if "scoring" not in config:
            await conn.execute("INSERT INTO config (key, value) VALUES ($1, $2)", "scoring", "true")
        if "flags_on" not in config:
            await conn.execute("INSERT INTO config (key, value) VALUES ($1, $2)", "flags_on", "true")


@app.on_event("shutdown")
async def on_shutdown():
    await app.state.db_pool.close()


app.state.sub_apps = []
for mod_name in modules:
    mod = importlib.import_module("ractf_backend.routers." + mod_name)
    sub_app = mod.app
    sub_app.debug = debug
    if debug:
        mod.app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"])
    app.state.sub_apps.append(sub_app)

    app.mount("/" + mod_name, sub_app)
