# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# common db things
# this is currently pooling

from os import environ

import asyncpg

from .config import config


async def get_pool():
    if "RACTF_TEST_ACTIVE" in environ:
        db_uri = config.get("db", "test_uri")
    else:
        db_uri = config.get("db", "uri")
    pool = await asyncpg.create_pool(db_uri)
    return pool
