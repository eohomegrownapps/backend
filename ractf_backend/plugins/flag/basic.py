from ..bases import FlagTypePlugin


class BasicFlagPlugin(FlagTypePlugin):
    def check(self, attempt):
        return attempt == self.flag_info["flag"]


PLUGIN = BasicFlagPlugin
