import hashlib

from ..bases import FlagTypePlugin


def get_hash(s):
    # returns the sha256 hex digest as a string of the string passed.
    return hashlib.sha256(s.encode("utf-8")).hexdigest()


class HashedFlagPlugin(FlagTypePlugin):
    # works by comparing the value in the database with the sha256 hash of the
    # utf-8 encoded representation of the submitted value.
    def check(self, attempt):
        return get_hash(attempt) == self.flag_info["flag"]


PLUGIN = HashedFlagPlugin
