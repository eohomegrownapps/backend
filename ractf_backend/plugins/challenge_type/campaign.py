# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from ..bases import ChallengeTypePlugin


class CampaignChallenge(ChallengeTypePlugin):
    def serialise(self, *args, **kwargs):
        base = super().serialise(*args, **kwargs)

        metadata = {
            "x": self.metadata.get("x", 0),
            "y": self.metadata.get("y", 0),
        }

        base["metadata"] = metadata
        return base


PLUGIN = CampaignChallenge
