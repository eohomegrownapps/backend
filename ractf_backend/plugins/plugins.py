# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import importlib


class ConstructorDict(dict):
    # utility class to make the plugin construction syntax a little
    # nicer.

    # instead of plugins.challenge_type[record['type']](record)
    # you can now do plugins.challenge_type(record) for instance, because
    # the challenge_type dict is actually one of these.
    def __init__(self, *args, field_name="type", **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.__field_name = field_name

    def __call__(self, record):
        # for now we assume the field for which plugin type to use is always called 'type'
        # that might need to change in future, and it might not.
        return self[record[self.__field_name]](record)


class _Plugins:
    # global singletonish class for managing access to plugins and dispatching
    # based on type, etc

    def __init__(self):
        self.challenge_type = ConstructorDict()
        self.flag = ConstructorDict(field_name="flag_type")
        for n in ("campaign", "jeopardy"):
            # TODO: move that list somewhere more accessible
            mod = importlib.import_module("ractf_backend.plugins.challenge_type." + n)
            self.challenge_type[n] = mod.PLUGIN

        for n in ("basic", "hashed"):
            mod = importlib.import_module("ractf_backend.plugins.flag." + n)
            self.flag[n] = mod.PLUGIN


plugins = _Plugins()
