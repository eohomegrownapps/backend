# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json


class ChallengeTypePlugin:
    def __init__(self, record):
        # This corresponds to the schema
        for name in ("id", "type", "name", "metadata", "description", "author",
                     "base_score", "challenge_group", "flag_type", "auto_unlock",
                     "flag_info"):
            setattr(self, name, record[name])

        if self.metadata is None:
            self.metadata = {}
        else:
            self.metadata = json.loads(self.metadata)

    def serialise(self, solved=False, lock=False, deps=None, admin=False, files=None, hints=None):
        deps = [str(i["id"]) for i in deps]
        if not lock:
            ret = {
                "type": self.type,
                "id": str(self.id),
                "name": self.name,
                "description": self.description,
                "author": self.author,
                "base_score": self.base_score,
                "challenge_group": str(self.challenge_group),
                "flag_type": self.flag_type,
                "metadata": None,
                "solved": solved,
                "lock": False,
                "deps": deps,
                "auto_unlock": self.auto_unlock,
                "files": files or [],
                "hints": hints or [],
            }
        else:
            ret = {
                "id": str(self.id),
                "challenge_group": str(self.challenge_group),
                "metadata": None,
                "solved": solved,
                "lock": True,
                "deps": deps,
                "auto_unlock": self.auto_unlock,
            }
        if admin:
            ret["flag_type"] = self.flag_type
            ret["flag"] = self.flag_info

        return ret


class FlagTypePlugin:
    def __init__(self, record):
        # Note: the record is the challenge record itself, there is no separate flag table
        self.flag_info = json.loads(record['flag_info'])

    def check(self, attempt):
        raise NotImplementedError()
