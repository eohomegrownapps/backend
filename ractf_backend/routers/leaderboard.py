# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from starlette.applications import Starlette

from ..response_util import success

app = Starlette()


@app.route("/")
async def leaderboard(request):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        try:
            solves = await conn.fetch("""
                SELECT solves.user_id as u_id, challenges.id AS c_id, challenges.name AS c_name,
                       solves.solve_timestamp,
                       base_score - (
                           SELECT COALESCE(SUM(hints.penalty), 0) FROM hints_used
                           JOIN hints ON hints_used.hint = hints.id
                           WHERE hints_used.user_id = user_id and hints.challenge_id = solves.challenge
                       ) AS score, users.name, users.team as team_id, teams.name as team_name
                FROM solves
                    INNER JOIN users ON users.id = solves.user_id
                    INNER JOIN teams ON users.team = teams.id
                    JOIN challenges ON challenges.id = solves.challenge
                WHERE users.visible AND teams.visible
            """)
        except:
            import traceback
            traceback.print_exc()
            return success([])

    solves = [
        dict(
            user_id=str(i["u_id"]),
            name=i["name"],
            team_id=str(i["team_id"]),
            team_name=str(i["team_name"]),
            challenge=str(i["c_id"]),
            challenge_name=i["c_name"],
            time=str(i["solve_timestamp"]),
            points=i["score"],
        )
        for i in solves
    ]

    return success(solves)
