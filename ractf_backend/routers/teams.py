import uuid
from json.decoder import JSONDecodeError

from starlette.applications import Starlette
from starlette.authentication import requires
from starlette.endpoints import HTTPEndpoint
from starlette.middleware.authentication import AuthenticationMiddleware

from .auth import TokenAuthBackend
from ..db import get_pool
from ..response_util import abort, success
from ..utils import json_params, limiter

app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/create", methods=["POST"])
@limiter("2/20m")
@requires("authenticated")
@json_params("name", "password")
# Plain text password to simplify inviting users to teams
async def create_team(request, name, password):
    if not (name and password):
        return abort(400, "Name and password must not be empty")
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        user_id = request.user.user_id
        if await conn.fetchval("SELECT team FROM users WHERE id = $1", user_id):
            return abort(400, "You are already in a team")
        if await conn.fetchval("SELECT id FROM teams WHERE name = $1", name):
            # Team enumeration is not an issue, since all teams are displayed on the leaderboard.
            return abort(400, "This team name is already in use")
        team_id = uuid.uuid4()
        await conn.execute(
            "INSERT INTO teams(id, name, upper_name, password, owner) VALUES ($1, $2, $3, $4, $5)",
            team_id,
            name,
            name.upper(),
            password,
            user_id,
        )
        await conn.execute("UPDATE users SET team = $1 WHERE id = $2", team_id, user_id)
        return success(
            {
                "id": str(team_id),
                "name": name,
                "password": password,
                "owner": {"id": str(user_id), "name": request.user.display_name},
            }
        )


@app.route("/join", methods=["POST"])
@requires("authenticated")
@limiter("15/20m")
@json_params("name", "password")
async def join_team(request, name, password):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        user_id = request.user.user_id
        if await conn.fetchval("SELECT team FROM users WHERE id = $1", user_id):
            return abort(400, "You are already in a team")
        team_id = await conn.fetchval(
            "SELECT id FROM teams WHERE name = $1 and password = $2", name, password
        )
        if not team_id:
            return abort(400, "Invalid team name or password")
        await conn.execute("UPDATE users SET team = $1 WHERE id = $2", team_id, user_id)
        return success({})


async def fetch_team_info(team_id):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        team_name, team_pass, owner_id, owner_name, description = await conn.fetchrow(
            """SELECT teams.name, password, owner, users.name AS owner_name,
                      teams.description
            FROM teams JOIN users ON teams.owner = users.id
            WHERE teams.id = $1""",
            team_id,
        )
        members_list = list(
            map(
                dict,
                await conn.fetch("SELECT users.id, users.name FROM users WHERE users.team = $1", team_id),
            )
        )
        members_list = [{"id": str(d["id"]), "name": d["name"]} for d in members_list]
        solves = await conn.fetch("""
            SELECT users.name as user_name, challenges.name, solves.solve_timestamp,
                   base_score - (
                       SELECT COALESCE(SUM(hints.penalty), 0) FROM hints_used
                       JOIN hints ON hints_used.hint = hints.id
                       WHERE hints_used.user_id = user_id AND hints.challenge_id = solves.challenge
                   ) AS score
            FROM solves
                   INNER JOIN users ON users.id = solves.user_id
                   INNER JOIN teams ON users.team = teams.id
                   JOIN challenges ON challenges.id = solves.challenge
            WHERE teams.id = $1
        """, str(team_id))
        solves = list(map(dict, solves))
        for i in solves:
            for j in i:
                i[j] = str(i[j])
        return {
            "id": str(team_id),
            "name": team_name,
            "description": description,
            "password": team_pass,
            "solves": solves,
            "owner": {"id": str(owner_id), "name": owner_name},
            "members": members_list,
        }


@app.route("/self")
class CurrentTeam(HTTPEndpoint):
    @requires("authenticated")
    async def get(self, request):
        pool = app.state.db_pool
        async with pool.acquire() as conn:
            team_id = await conn.fetchval("SELECT team FROM users WHERE id = $1", request.user.user_id)
            if not team_id:
                return abort(404, "You are not in a team")
            team_data = await fetch_team_info(team_id)
            return success(team_data)

    @requires("authenticated")
    async def patch(self, request):
        try:
            data = await request.json()
        except JSONDecodeError:
            return abort(400)
        fields = ["name", "password", "owner"]
        for key in fields:
            if key in data.keys() and not data[key]:
                return abort(400, f"The {key} field must not be empty if present in the request")
        user_id = request.user.user_id
        pool = app.state.db_pool
        async with pool.acquire() as conn:
            team_id = await conn.fetchval("SELECT team FROM users WHERE id = $1", user_id)
            if not team_id:
                return abort(400, "You are not in a team")
            queued_ops = (
                []
            )  # Operators are queued so that validation can take place for all fields in request.
            if await conn.fetchval("SELECT owner FROM teams WHERE id = $1", team_id) != user_id:
                return abort(403, "Only the owner is allowed to make changes to the team")
            if "name" in data.keys():
                new_name = data["name"]
                if await conn.fetchval("SELECT id FROM teams WHERE name = $1", new_name):
                    return abort(400, "There is an existing team with this name")
                queued_ops.append(conn.execute("UPDATE teams SET name = $1 WHERE id = $2", new_name, team_id))
            if "password" in data.keys():
                new_pass = data["password"]
                queued_ops.append(
                    conn.execute("UPDATE teams SET password = $1 WHERE id = $2", new_pass, team_id)
                )
            if "owner" in data.keys():
                try:
                    new_owner = uuid.UUID(data["owner"])
                except ValueError:
                    return abort(400, "Invalid UUID")
                if not await conn.fetchval(
                    "SELECT id FROM users WHERE team = $1 AND id = $2", team_id, new_owner
                ):
                    return abort(400, "The user being promoted to owner is not in the team")
                queued_ops.append(
                    conn.execute("UPDATE teams SET owner = $1 WHERE id = $2", new_owner, team_id)
                )
            for op in queued_ops:
                await op
            team_data = await fetch_team_info(team_id)
            return success(team_data)


@app.route("/list")
async def team_list(request):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        team_list = list(map(dict, await conn.fetch("SELECT id, name FROM teams WHERE visible=true")))
        team_list = [{"id": str(d["id"]), "name": d["name"]} for d in team_list]
        return success(team_list)


@app.route("/mod/{team_id}", methods=["PATCH"])
@limiter("15/20m")
@json_params("name", "description", "password", "captain",
             name=None, description=None, password=None, captain=None)
@requires("authenticated")
async def modify_team(request, name, description, password, captain):
    try:
        team_id = uuid.UUID(request.path_params["team_id"])
    except ValueError:
        return abort(403, "Invalid UUID")

    if not request.user.admin and team_id != request.user.team:
        return abort(403)

    params = {}
    if name is not None:
        name = str(name)
        if len(name) > 36:
            return abort(400, "Name too long")
        params["name"] = name
    if description is not None:
        description = str(description)
        if len(description) > 400:
            return abort(400, "Description too long")
        params["description"] = description
    if password is not None:
        password = str(password)
        if len(password) < 8:
            return abort(400, "Password too short")
        params["password"] = password
    if captain is not None:
        try:
            cuid = uuid.UUID(captain)
        except ValueError:
            return abort(400, "Invalid user ID")

        async with app.state.db_pool.acquire() as conn:
            memb = await conn.fetch("SELECT * FROM users WHERE id=$1 AND team=$2", cuid, team_id)
        if not memb:
            return abort(400, "Member not in team")
        params["owner"] = captain

    params = list(params.items())
    if not params:
        return success("")

    query = ",".join(f"{i[0]}=${n+2}" for n, i in enumerate(params))

    async with app.state.db_pool.acquire() as conn:
        await conn.execute(
            f"""
            UPDATE teams SET {query}
            WHERE id=$1
        """,  # noqa S608
            team_id,
            *[i[1] for i in params],
        )
    return success("")


@app.route("/{team_id}")
async def team_info(request):
    try:
        team_id = uuid.UUID(request.path_params["team_id"])
    except ValueError:
        return abort(400, "Invalid UUID")
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        if not await conn.fetchval("SELECT name FROM teams WHERE id = $1", team_id):
            return abort(404, "There is not a team with that information")
        team_data = await fetch_team_info(team_id)
        return success(team_data)
