# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import uuid

import bcrypt

from starlette.applications import Starlette
from starlette.authentication import requires
from starlette.middleware.authentication import AuthenticationMiddleware

from zxcvbn import zxcvbn

from .auth import TokenAuthBackend
from ..response_util import abort, success
from ..utils import json_params, limiter

app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/edit", methods=["POST"])
@json_params("id", "name", "cost", "body")
@requires("authenticated")
async def edit(request, id, name, cost, body):
    if not request.user.admin:
        return abort(403)

    try:
        id = uuid.UUID(str(id))
    except ValueError:
        return abort(400, "Invalid UUID")
    try:
        cost = int(cost)
    except ValueError:
        return abort(400, "Invalid cost")
    name, body = str(name).strip(), str(body).strip()
    if len(name) == 0:
        return abort(400, "Invalid name")
    if len(body) == 0:
        return abort(400, "Invalid body")

    async with app.state.db_pool.acquire() as conn:
        await conn.execute("""
            UPDATE hints SET name=$1, penalty=$2, message=$3
            WHERE id=$4
        """, name, cost, body, id)
    return success({})


@app.route("/new", methods=["POST"])
@json_params("chal_id", "name", "cost", "body")
@requires("authenticated")
async def new(request, chal_id, name, cost, body):
    if not request.user.admin:
        return abort(403)

    try:
        chal_id = uuid.UUID(str(chal_id))
    except ValueError:
        return abort(400, "Invalid UUID")
    try:
        cost = int(cost)
    except ValueError:
        return abort(400, "Invalid cost")
    name, body = str(name).strip(), str(body).strip()
    if len(name) == 0:
        return abort(400, "Invalid name")
    if len(body) == 0:
        return abort(400, "Invalid body")

    id = uuid.uuid4()

    async with app.state.db_pool.acquire() as conn:
        res = await conn.fetch("SELECT 1 FROM challenges WHERE id=$1", chal_id)
        if not res:
            return abort(404, "Challenge not found")

        await conn.execute("""
            INSERT INTO hints(id, challenge_id, message, penalty, name)
            VALUES ($1, $2, $3, $4, $5)
        """, id, chal_id, body, cost, name)

    return success(dict(
        id=str(id),
        name=name,
        cost=cost,
        body=body,
    ))

@app.route("/use", methods=["POST"])
@json_params("id")
@requires("authenticated")
async def use(request, id):
    try:
        id = uuid.UUID(str(id))
    except ValueError:
        return abort(400, "Invalid UUID")

    async with app.state.db_pool.acquire() as conn:
        hint = await conn.fetch("SELECT message FROM hints WHERE id=$1", id)

    if not hint:
        return abort(404, "Hint not found")
    hint = hint[0]

    async with app.state.db_pool.acquire() as conn:
        used = await conn.fetch("SELECT 1 FROM hints_used WHERE hint=$1 AND user_id=$2", id, request.user.user_id)
        if used:
            return abort(400, "Hint already used")

        await conn.execute("""
            INSERT INTO hints_used (hint, user_id)
            VALUES ($1, $2)
        """, id, request.user.user_id)

    return success(hint["message"])

