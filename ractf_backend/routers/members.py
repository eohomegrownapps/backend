# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import uuid

import bcrypt

from starlette.applications import Starlette
from starlette.authentication import requires
from starlette.middleware.authentication import AuthenticationMiddleware

from zxcvbn import zxcvbn

from .auth import TokenAuthBackend
from ..response_util import abort, success
from ..utils import json_params, limiter

app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/self")
@requires("authenticated")
async def self(request):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow(
            """
            SELECT totp_status, bio, discord, twitter, reddit, discordid, joined
            FROM users WHERE id=$1
        """,
            request.user.user_id,
        )
        return success(
            {
                "id": str(request.user.user_id),
                "bio": row["bio"],
                "social": {
                    "discord": row["discord"],
                    "discordid": row["discordid"],
                    "twitter": row["twitter"],
                    "reddit": row["reddit"],
                },
                "joined": row["joined"].isoformat(),
                "username": request.user.display_name,
                "is_admin": request.user.admin,
                "2fa_status": row["totp_status"],
            }
        )


@app.route("/list")
async def member_list(request):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        rows = await conn.fetch("""
            SELECT users.id, users.name, teams.name AS team_name
            FROM users LEFT JOIN teams ON users.team = teams.id
            WHERE users.visible=true
        """)
        return success(
            [{"id": str(row["id"]), "name": row["name"], "team_name": row["team_name"]} for row in rows]
        )


@app.route("/mod/{user_id}", methods=["PATCH"])
@limiter("15/7m")
@json_params(
    "oPass", "nPass", "name", "discord",
    "twitter", "reddit", "bio", "discordid",
    oPass=None, nPass=None, name=None, discord=None,
    twitter=None, reddit=None, bio=None, discordid=None,
)
@requires("authenticated")
async def modify_user(request, oPass, nPass, name, discord, twitter, reddit, bio, discordid):
    try:
        user_id = uuid.UUID(request.path_params["user_id"])
    except ValueError:
        return abort(400, "Invalid UUID")

    if not request.user.admin and user_id != request.user.user_id:
        return abort(403)

    params = {}
    if name is not None:
        name = str(name)
        if len(name) < 4:
            return abort(400, "Username too short")
        if len(name) > 20:
            return abort(400, "Username too long")
        pool = app.state.db_pool
        async with pool.acquire() as conn:
            row = await conn.fetchrow(
                "SELECT email, upper_name FROM users WHERE name = $1", name
            )
            if row is not None:
                return abort(400, "Name already in use")

        params["name"] = name
    if discord is not None:
        discord = str(discord)
        if len(discord) > 36:
            return abort(400, "Discord too long")
        params["discord"] = discord
    if twitter is not None:
        twitter = str(twitter).lstrip("@")
        if len(twitter) > 36:
            return abort(400, "Twitter too long")
        params["twitter"] = twitter
    if reddit is not None:
        reddit = str(reddit)
        if reddit.startswith("/u/"):
            reddit = reddit[3:]
        if len(reddit) > 36:
            return abort(400, "Reddit too long")
        params["reddit"] = reddit
    if bio is not None:
        bio = str(bio)
        if len(bio) > 400:
            return abort(400, "Bio too long")
        params["bio"] = bio
    if discordid is not None:
        discordid = str(discordid)
        if not 16 < len(discordid) < 20:
            return abort(400, "Invalid user ID")
        params["discordid"] = discordid
    if nPass is not None:
        nPass, oPass = str(nPass), str(oPass)
        results = zxcvbn(nPass, user_inputs=[name, "ractf"])
        if results["score"] < 3:
            return abort(400, "Password is too insecure. ")
        if oPass is None:
            return abort(400, "Old password required")

        async with app.state.db_pool.acquire() as conn:
            hashed_pw = await conn.fetchval(
                """
                SELECT hashed_password FROM users WHERE id=$1
            """,
                request.user.user_id,
            )

        if not bcrypt.checkpw(oPass.encode("utf-8"), hashed_pw.encode("utf-8")):
            return abort(400, "Incorrect password")

        nPass = bcrypt.hashpw(nPass.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")
        params["hashed_password"] = nPass

    params = list(params.items())
    if not params:
        return success("")

    query = ",".join(f"{i[0]}=${n+2}" for n, i in enumerate(params))

    async with app.state.db_pool.acquire() as conn:
        await conn.execute(
            f"""
            UPDATE users SET {query}
            WHERE id=$1
        """,  # noqa S608
            user_id,
            *[i[1] for i in params],
        )

    return success("")


@app.route("/{user_id}")
async def user_info(request):
    try:
        user_id = uuid.UUID(request.path_params["user_id"])
    except ValueError:
        return abort(400, "Invalid UUID")

    async with app.state.db_pool.acquire() as conn:
        user_dat = await conn.fetchrow(
            """
            SELECT users.id AS user_id, users.name AS user_name, users.admin,
                   teams.name AS team_name, teams.id AS team_id,
                   users.bio, users.discord, users.twitter, users.reddit,
                   users.discordid, users.joined
            FROM users LEFT JOIN teams on users.team = teams.id
            WHERE users.id=$1
        """,
            user_id,
        )
        if not user_dat:
            return abort(404)
        solves = await conn.fetch(
            """
            SELECT challenges.id, challenges.name, solves.solve_timestamp, challenges.base_score - (
                SELECT COALESCE(SUM(hints.penalty), 0) FROM hints_used
                JOIN hints ON hints_used.hint = hints.id
                WHERE hints_used.user_id = user_id AND hints.challenge_id = solves.challenge
            ) AS score
            FROM solves JOIN challenges on solves.challenge = challenges.id
            WHERE solves.user_id=$1
        """,
            user_id,
        )

        solves = [
            dict(
                id=str(i["id"]),
                name=i["name"],
                time=str(i["solve_timestamp"]),
                points=i["score"],
            )
            for i in solves
        ]

        return success(
            dict(
                id=str(user_dat["user_id"]),
                username=user_dat["user_name"],
                is_admin=user_dat["admin"],
                team=dict(id=str(user_dat["team_id"]), name=user_dat["team_name"]),
                bio=user_dat["bio"],
                social=dict(
                    discord=user_dat["discord"],
                    discordid=user_dat["discordid"],
                    twitter=user_dat["twitter"],
                    reddit=user_dat["reddit"],
                ),
                joined=user_dat["joined"].isoformat(),
                solves=solves,
            )
        )
