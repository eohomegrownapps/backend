# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import uuid

import bcrypt

from starlette.applications import Starlette
from starlette.authentication import requires
from starlette.middleware.authentication import AuthenticationMiddleware

from .auth import TokenAuthBackend
from ..response_util import abort, success
from ..utils import json_params

app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/new", methods=["POST"])
@json_params("name", "desc", "type")
@requires("authenticated")
async def new(request, name, desc, type):
    if not request.user.admin:
        return abort(403)

    new_id = uuid.uuid4()
    async with app.state.db_pool.acquire() as conn:
        cats = await conn.fetchval("""
            SELECT COUNT(*) FROM challenge_groups
        """)
        await conn.execute("""
            INSERT INTO challenge_groups
            (id, name, description, display_order, contained_type)
            VALUES ($1, $2, $3, $4, $5)
        """, new_id, name, desc, cats, type)
    return success({})


@app.route("/edit", methods=["POST"])
@json_params("id", "name", "desc", "type")
@requires("authenticated")
async def edit(request, id, name, desc, type):
    if not request.user.admin:
        return abort(403)

    try:
        id = uuid.UUID(id)
    except ValueError:
        return abort(400, "Invalid UUID")

    async with app.state.db_pool.acquire() as conn:
        curr = await conn.fetch("""
            SELECT name, description, contained_type FROM challenge_groups
            WHERE id=$1
        """, id)
        if not curr:
            return abort(404)

        await conn.execute("""
            UPDATE challenge_groups
            SET name=$1, description=$2, contained_type=$3
            WHERE id=$4
        """, name, desc, type, id)
    return success({})
