# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import uuid

import bcrypt

from starlette.applications import Starlette
from starlette.authentication import requires
from starlette.middleware.authentication import AuthenticationMiddleware

from zxcvbn import zxcvbn

from .auth import TokenAuthBackend
from ..response_util import abort, success
from ..utils import json_params, limiter

app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/config", methods=["GET", "POST"])
async def config(request):
    async with app.state.db_pool.acquire() as conn:
        res = await conn.fetch(
            """
                SELECT key, value FROM config
                WHERE key='login' OR key='registration' OR key='flags_on'
            """
        )
        return success({
            i["key"]: json.loads(i["value"]) for i in res
        })


@app.route("/admin_config", methods=["GET", "POST"])
@json_params("key", "value", key=None, value=None)
@requires("authenticated")
async def admin_config(request, key, value):
    if not request.user.admin:
        return abort(403)

    if request.method == "GET":
        async with app.state.db_pool.acquire() as conn:
            res = await conn.fetch(
                """
                    SELECT key, value FROM config
                """
            )
            return success({
                i["key"]: json.loads(i["value"]) for i in res
            })
    else:
        if key is None or value is None:
            return abort(400)
        async with app.state.db_pool.acquire() as conn:
            if not await conn.fetchval("SELECT 1 FROM config WHERE key=$1", key):
                return abort(404, "No such config")
            await conn.execute("""
                UPDATE config SET value=$2 WHERE key=$1
            """, key, json.dumps(value))
            if key == "scoring":
                return abort(400, "Warning: This setting does nothing currently")
            return success({})


@app.route("/teams")
@requires("authenticated")
async def teams_list(request):
    if not request.user.admin:
        return abort(403)

    async with app.state.db_pool.acquire() as conn:
        rows = await conn.fetch("""
            SELECT teams.id, teams.name, teams.visible
            FROM teams
        """)
        return success([
            {
                "id": str(row["id"]),
                "name": row["name"],
                "visible": row["visible"],
            } for row in rows
        ])


@app.route("/edit_team", methods=["POST"])
@json_params("id", "visible", visible=None)
@requires("authenticated")
async def edit_team(request, id, visible):
    if not request.user.admin:
        return abort(403)

    try:
        id = uuid.UUID(id)
    except ValueError:
        return abort(400, "Invalid UUID")

    if visible is None:
        return success({})

    async with app.state.db_pool.acquire() as conn:
        if visible is not None:
            await conn.execute("""
                UPDATE teams SET visible=$2 WHERE id=$1
            """, id, visible)

    return success({})


@app.route("/members")
@requires("authenticated")
async def members_list(request):
    if not request.user.admin:
        return abort(403)

    async with app.state.db_pool.acquire() as conn:
        rows = await conn.fetch("""
            SELECT users.id, users.name, users.enabled, users.visible,
                   teams.name AS team_name, teams.id as team_id, admin
            FROM users LEFT JOIN teams ON users.team = teams.id
        """)
        return success([
            {
                "id": str(row["id"]),
                "name": row["name"],
                "enabled": row["enabled"],
                "visible": row["visible"],
                "team_id": str(row["team_id"]),
                "team_name": row["team_name"],
                "is_admin": row["admin"],
            } for row in rows
        ])


@app.route("/edit_member", methods=["POST"])
@json_params("id", "enabled", "visible", enabled=None, visible=None)
@requires("authenticated")
async def edit_member(request, id, enabled, visible):
    if not request.user.admin:
        return abort(403)

    try:
        id = uuid.UUID(id)
    except ValueError:
        return abort(400, "Invalid UUID")

    if enabled is None and visible is None:
        return success({})

    async with app.state.db_pool.acquire() as conn:
        if enabled is not None:
            await conn.execute("""
                UPDATE users SET enabled=$2 WHERE id=$1
            """, id, enabled)
        if visible is not None:
            await conn.execute("""
                UPDATE users SET visible=$2 WHERE id=$1
            """, id, visible)

    return success({})
