# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import uuid
from datetime import datetime

from starlette.applications import Starlette
from starlette.authentication import requires
from starlette.middleware.authentication import AuthenticationMiddleware

from .auth import TokenAuthBackend
from ..plugins.plugins import plugins
from ..response_util import abort, success
from ..utils import json_params

app = Starlette()
app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/", methods=["GET"])
@requires("authenticated")
async def list_all(request):
    """Lists all challenges that we know about.

    This isn't finished at all yet. It provides too much information (eg, flags!) for challenges
    that are still locked (in fact, it doesn't even check if they're locked, because we don't have
    plugins yet), and it doesn't provide any info like "has this challenge been
    solved by the current user yet" either. That will come soon.
    """
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        challenges = await conn.fetch("SELECT * FROM challenges")
        links = await conn.fetch("SELECT required, unlocks FROM challenge_links")
        solves = {i["challenge"] for i in await conn.fetch("""
            SELECT challenge FROM solves WHERE user_id=$1
        """, request.user.user_id)}

    if not challenges:
        return success([])
    challenges_lookup = {i["id"]: i for i in challenges}

    def dependencies(chal):
        return [challenges_lookup[i["required"]] for i in links if str(i["unlocks"]) == str(chal["id"])]

    def is_solved(chal):
        if isinstance(chal["id"], str):
            return uuid.UUID(chal["id"]) in solves
        return chal["id"] in solves

    def is_unlocked(chal):
        if request.user.admin:
            return True
        if chal["auto_unlock"]:
            return True
        if is_solved(chal):
            return True

        deps = dependencies(chal)
        if not deps:
            return True

        return any(is_solved(i) for i in deps)

    async with pool.acquire() as conn:
        group_data = await conn.fetch("""
            SELECT id, display_order, name, description, contained_type as type, metadata
            FROM challenge_groups
        """)
        files = await conn.fetch("""
            SELECT challenge, name, url, size, id FROM files
        """)
        hints = await conn.fetch("""
            SELECT name, penalty, message, id, challenge_id FROM hints
        """)
        hints_used = await conn.fetch("""
            SELECT hint FROM hints_used WHERE user_id=$1
        """, request.user.user_id)
    hints_used = [i["hint"] for i in hints_used]

    group_data.sort(key=lambda x: x["display_order"])
    group_lookup = {i["id"]: i for i in group_data}

    challenge_data = []
    for record in challenges:
        group = group_lookup.get(record["challenge_group"])
        if group is None:
            continue

        c_files = [dict(
            id=str(i["id"]),
            name=i["name"],
            url=i["url"],
            size=i["size"],
        ) for i in files if i["challenge"] == record["id"]]

        c_hints = [dict(
            id=str(i["id"]),
            name=i["name"],
            cost=i["penalty"],
            body=i["message"] if request.user.admin or i["id"] in hints_used else "",
            hint_used=i["id"] in hints_used,
        ) for i in hints if i["challenge_id"] == record["id"]]
        
        solved, lock = is_solved(record), not is_unlocked(record)

        plugin = plugins.challenge_type[group["type"]](record)
        challenge_data.append(plugin.serialise(solved=solved, lock=lock, deps=dependencies(record),
                                               admin=request.user.admin, files=c_files, hints=c_hints))

    challenge_tree = [
        {
            "id": str(i["id"]),
            "name": i["name"],
            "type": i["type"],
            "desc": i["description"],
            "meta": i["metadata"],
            "chals": [
                j for j in challenge_data if str(j["challenge_group"]) == str(i["id"])
            ]
        } for i in group_data
    ]

    return success(challenge_tree)


@app.route("/link", methods=["POST"])
@json_params("cfrom", "cto", "state")
@requires("authenticated")
async def link(request, cfrom, cto, state):
    if not request.user.admin:
        return abort(403)

    async with app.state.db_pool.acquire() as conn:
        if state:
            try:
                cfrom = await conn.fetch("""
                    INSERT INTO challenge_links (required, unlocks)
                    VALUES ($1, $2)
                """, cfrom, cto)
            except:
                raise
        else:
            await conn.execute("""
                DELETE FROM challenge_links
                WHERE required=$1 AND unlocks=$2
            """, cfrom, cto)
    return success({})


@app.route("/edit", methods=["POST"])
@json_params("id", "name", "points", "desc", "flag_type", "flag", "auto_unlock", "meta")
@requires("authenticated")
async def edit(request, id, name, points, desc, flag_type, flag, auto_unlock, meta):
    if not request.user.admin:
        return abort(403)

    try:
        id = uuid.UUID(id)
    except ValueError:
        return abort(400, "Invalid UUID")
    if not isinstance(name, str) or not name:
        return abort(400, "Invalid name")
    try:
        points = int(points)
    except ValueError:
        return abort(400, "Invalid points value")
    if not isinstance(desc, str):
        return abort(400, "Invalid brief")

    try:
        auto_unlock = bool(auto_unlock)
    except ValueError:
        return abort(400, "Invalid auto unlock value")

    flag_type = str(flag_type).strip()
    if str(flag).strip() and flag_type:
        if flag_type not in plugins.flag:
            return abort(400, "Invalid flag type.\nInstalled plugins: " + ", ".join(plugins.flag))
    elif str(flag).strip() and not flag_type or flag_type and not str(flag).strip():
        return abort(400, "Both or neither of flag info and type must be provided")

    async with app.state.db_pool.acquire() as conn:
        curr = await conn.fetch("""
            SELECT 1 FROM challenges
            WHERE id=$1
        """, id)
        if not curr:
            return abort(404)

        if flag_type:
            await conn.execute("""
                UPDATE challenges
                SET name=$1, base_score=$2, description=$3, flag_info=$4, metadata=$5,
                    flag_type=$6, auto_unlock=$7
                WHERE id=$8
            """, name, points, desc, json.dumps(flag), json.dumps(meta), flag_type, auto_unlock, id)
        else:
            await conn.execute("""
                UPDATE challenges
                SET name=$1, base_score=$2, description=$3, metadata=$4, auto_unlock=$5
                WHERE id=$6
            """, name, points, desc, json.dumps(meta), auto_unlock, id)
    return success({})


@app.route("/new", methods=["POST"])
@json_params("group", "name", "points", "desc", "flag_type", "flag", "auto_unlock", "meta")
@requires("authenticated")
async def new(request, group, name, points, desc, flag_type, flag, auto_unlock, meta):
    if not request.user.admin:
        return abort(403)

    new_id = uuid.uuid4()
    try:
        group = uuid.UUID(group)
    except ValueError:
        return abort(400, "Invalid UUID")

    if not isinstance(name, str) or not name:
        return abort(400, "Invalid name")
    try:
        points = int(points)
    except ValueError:
        return abort(400, "Invalid points value")
    if not isinstance(desc, str):
        return abort(400, "Invalid brief")

    try:
        auto_unlock = bool(auto_unlock)
    except ValueError:
        return abort(400, "Invalud auto unlock value")

    flag_type = str(flag_type)
    if flag_type not in plugins.flag:
        return abort(400, "Invalid flag type.\nInstalled plugins: " + ", ".join(plugins.flag))

    async with app.state.db_pool.acquire() as conn:
        curr = await conn.fetch("""
            SELECT 1 FROM challenge_groups
            WHERE id=$1
        """, group)
        if not curr:
            return abort(404, "No such group")

        await conn.execute("""
            INSERT INTO challenges (
                id, challenge_group, metadata, name, description,
                base_score, flag_type, flag_info, auto_unlock
            ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        """, new_id, group, json.dumps(meta), name, desc, points, flag_type, json.dumps(flag), auto_unlock)
    return success({})


@app.route("/{challenge_id:str}", methods=["GET"])
@requires("authenticated")
async def challenge_details(request):
    """Provide information about a specific challenge.

    This will probably provide a little more info than the "list all" endpoint.
    (For example, hints). What exactly the difference between them, in terms of
    what information is provided, is yet to be fully decided upon.
    """
    challenge_id = request.path_params["challenge_id"]
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        challenge = await conn.fetchrow("SELECT * FROM challenges WHERE id = $1", challenge_id)
        hints = await conn.fetch("SELECT * FROM hints WHERE challenge_id = $1", challenge_id)
        group_type = await conn.fetchval("SELECT contained_type FROM challenge_groups WHERE id = $1",
                                         challenge["challenge_group"])
        links = await conn.fetch("SELECT 1 FROM challenge_links WHERE unlocks=$1 LIMIT 1", challenge_id)
        solved = len(await conn.fetch("""
            SELECT 1 FROM solves WHERE challenge=$1 AND user_id=$1 LIMIT 1
        """, challenge_id, request.user.user_id)) > 1
        if links:
            dependencies = await conn.fetch("""
                SELECT 1 FROM solves
                JOIN challenge_links ON solves.challenge=challenge_links.required
                WHERE challenge_links.unlocks=$1 LIMIT 1
            """, challenge_id)
        else:
            dependencies = [True]

    hints_list = [{"id": str(h["id"]), "penalty": h["penalty"]} for h in hints]

    result = {
        "challenge": plugins.challenge_type[group_type](challenge).serialise(solved, lock=not any(dependencies)),
    }
    if any(dependencies):
        result["hints"] = hints_list

    return success(result)


@app.route("/{challenge_id:str}/attempt", methods=["POST"])
@requires("authenticated")
@json_params("flag")
async def attempt_challenge(request, flag):
    pool = app.state.db_pool
    if not request.user.admin:
        async with pool.acquire() as conn:
            flags_enabled = json.loads(await conn.fetchval("SELECT value FROM config WHERE key='flags_on'"))
        if not flags_enabled:
            return abort(403, "Flag submission disabled")

    try:
        challenge_id = uuid.UUID(request.path_params['challenge_id'])
    except ValueError:
        return abort(400)

    async with pool.acquire() as conn:
        if await conn.fetch("""
            SELECT 1 FROM solves WHERE challenge=$1 AND user_id=$2
        """, challenge_id, request.user.user_id):
            return abort(400, "You have already solved this challenge")

    async with pool.acquire() as conn:
        challenge = await conn.fetchrow("SELECT * FROM challenges WHERE id = $1", challenge_id)
        if challenge["auto_unlock"]:
            solves = [True]
        else:
            # The next two queries can't be merged as the edge-case of a challenge
            # with no dependencies must be handled
            links = await conn.fetch("""
                SELECT 1 FROM challenge_links
                WHERE unlocks=$1 LIMIT 1
            """, challenge_id)
            if links:
                solves = await conn.fetch("""
                    SELECT 1 FROM solves
                    JOIN challenge_links ON solves.challenge=challenge_links.required
                    WHERE challenge_links.unlocks=$1 LIMIT 1
                """, challenge_id)
            else:
                solves = [True]

    if not any(solves):
        return abort(403)

    flag_plugin = plugins.flag(challenge)

    if flag_plugin.check(flag):
        # TODO: record the correct solution
        async with pool.acquire() as conn:
            await conn.execute("""
                INSERT INTO solves (user_id, challenge, solve_timestamp) VALUES ($1, $2, $3)
            """, request.user.user_id, challenge_id, datetime.now())
        return success({"correct": True})
    else:
        return success({"correct": False})
