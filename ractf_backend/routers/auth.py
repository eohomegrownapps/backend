# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime
import hashlib
import hmac
import json
import re
from uuid import UUID, uuid4

import bcrypt

import pyotp

from starlette.applications import Starlette
from starlette.authentication import (AuthCredentials, AuthenticationBackend,
                                      AuthenticationError, BaseUser, requires)
from starlette.middleware.authentication import AuthenticationMiddleware

from zxcvbn import zxcvbn

from ..config import config
from ..response_util import abort, success
from ..utils import json_params, limiter, send_email

HMAC_KEY = config.get("secrets", "key")
VERIF_EMAIL_BASE = config.get("email", "verif_base")
RESET_EMAIL_BASE = config.get("email", "reset_base")
app = Starlette()


class TokenAuthBackend(AuthenticationBackend):
    async def authenticate(self, request):
        if "Authorization" not in request.headers:
            return
        try:
            user_id, token = request.headers["Authorization"].split(".")
            user_id = UUID(user_id)
        except ValueError:
            raise AuthenticationError({"s": False, "m": "Invalid token format"})

        if await validate_token(token, user_id):
            scope = ["authenticated"]
            admin = False
            pool = request.app.state.db_pool
            async with pool.acquire() as conn:
                user = (await conn.fetch("SELECT name, admin, enabled, team FROM users WHERE id = $1", user_id))[0]
                name = user["name"]
                team = user["team"]
                if user["admin"]:
                    scope.append("admin")
                    admin = True
            if not user["enabled"] and not admin:
                return
            return AuthCredentials(scope), RACTFUser(name, user_id, team, admin)
        return


class RACTFUser(BaseUser):
    def __init__(self, username, user_id, team=None, admin=False):
        self.username = username
        self.user_id = user_id
        self.admin = admin
        self.team = team

    @property
    def is_authenticated(self):
        return True

    @property
    def display_name(self):
        return self.username


async def validate_token(token, uuid):
    """
    A helper function.
    Used by the authentication backend i.e the bit that actually authenticates users.
    Also used by verify_token i.e the utility endpoint that checks tokens.
    """
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow("SELECT name, hashed_password FROM users WHERE id = $1", str(uuid))
        if row is not None:
            hm = hmac.new(HMAC_KEY.encode("utf-8"), digestmod=hashlib.sha3_512)
            hm.update(row[0].encode("utf-8"))
            hm.update(row[1].encode("utf-8"))
            server_token = hm.hexdigest()
            return hmac.compare_digest(server_token, token)
        return False


app.add_middleware(AuthenticationMiddleware, backend=TokenAuthBackend())


@app.route("/add_2fa", methods=["POST"])
@limiter("2/5m")
@requires(["authenticated"])
async def add_2fa(request):
    uuid = request.user.user_id
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        # Add 2fa to account
        totp_secret = pyotp.random_base32()
        await conn.execute("UPDATE users SET totp_secret = $1 WHERE id = $2", totp_secret, uuid)
        await conn.execute("UPDATE users SET totp_status = 'needs_verify' WHERE id = $1", uuid)
        return success({"totp_secret": totp_secret})


@app.route("/verify_2fa", methods=["POST"])
@limiter("5/5m")
@json_params("otp")
@requires(["authenticated"])
async def totp_verify(request, otp):
    uuid = request.user.user_id
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow("SELECT totp_secret, totp_status id FROM users WHERE id = $1", uuid)
        if row[1] != "needs_verify":
            return abort(400, "2FA not set up or already verified")
        totp_secret = row[0]
        totp = pyotp.TOTP(totp_secret)
        if not totp.verify(otp, valid_window=1):
            return abort(401, "Incorrect 2FA token")
        await conn.execute("UPDATE users SET totp_status = 'on' WHERE id = $1", uuid)
        return success({})


@app.route("/login", methods=["POST"])
@limiter("15/5m")
@json_params("username", "password", "otp", otp=None)
async def login(request, username, password, otp):
    # Strip Gmail + and . addresses
    email = username.strip().lower()
    if email.endswith("@gmail.com"):
        email = email.split("@")[0].split("+")[0].replace(".", "") + "@gmail.com"
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow("""
            SELECT hashed_password, email_verified, name, id, totp_secret, totp_status, admin, enabled
            FROM users
            WHERE name = $1 OR email = $2
        """, username, email)
        login_enabled = json.loads(await conn.fetchval("SELECT value FROM config WHERE key='login'"))

        if not login_enabled and not (row and row["admin"]):
            return abort(403, "Login disabled")

        if row is not None:
            if not row["enabled"]:
                return abort(403, "Account disabled")

            username = row["name"]
            if not row["email_verified"]:
                return abort(403, "Email unverified")

            hashed_pw = row["hashed_password"]
            if bcrypt.checkpw(password.encode("utf-8"), hashed_pw.encode("utf-8")):
                hm = hmac.new(HMAC_KEY.encode("utf-8"), digestmod=hashlib.sha3_512)
                hm.update(username.encode("utf-8"))
                hm.update(hashed_pw.encode("utf-8"))
                hm_token = hm.hexdigest()  # HMAC part of the token
                user_id = str(row["id"])  # UUID is returned as an UUID, rather than a string
                token = user_id + "." + hm_token  # Actual token
                if otp is not None and row["totp_status"] == "on":
                    totp = pyotp.TOTP(row["totp_secret"])
                    if not totp.verify(otp, valid_window=1):
                        return abort(401, "Incorrect 2FA token")
                elif row["totp_secret"] and row["totp_status"] == "on":
                    return abort(401, "2FA required")
                return success({"token": token})
            else:
                return abort(403, "Incorrect username/password")
        return abort(403, "Incorrect username/password")


@app.route("/register", methods=["POST"])
@limiter("3/15m")
@json_params("email", "password", "username")
async def register(request, email, password, username):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        reg_enabled = json.loads(await conn.fetchval("SELECT value FROM config WHERE key='registration'"))

    if not reg_enabled:
        return abort(403, "Registration disabled")

    # Validate user input
    # Username length
    if len(username) > 20:
        return abort(400, "Username too long")
    if len(username) < 4:
        return abort(400, "Username too short")
    if "@" in username:
        return abort(400, "Username cannot contain @")

    # Password
    results = zxcvbn(password, user_inputs=[username, "ractf"])
    if results["score"] < 3:
        return abort(400, "Password is too insecure. ")

    # Email
    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return abort(400, "Invalid email address.")

    # Strip Gmail + and . addresses
    email = email.strip().lower()
    if email.endswith("@gmail.com"):
        email = email.split("@")[0].split("+")[0].replace(".", "") + "@gmail.com"

    # Check user / email doesn't already exist
    async with pool.acquire() as conn:
        row = await conn.fetchrow(
            "SELECT email, upper_name FROM users WHERE email = $1 OR name = $2", email, username
        )
        if row is not None:
            if row[0] == email:
                return abort(400, "Email already in use")
            else:
                return abort(400, "Username already in use")

    # Add user to DB
    uuid = uuid4()  # Generate a UUID for the user; I'm not checking if it
    #                 exists because the likelihood it will is next to none
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        await conn.execute(
            """
            INSERT INTO users(id, name, upper_name, email, joined, hashed_password) VALUES ($1, $2, $3, $4, $5, $6)
        """,
            uuid,
            username,
            username.upper(),
            email,
            datetime.datetime.now(),
            bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt()).decode("utf-8"),
        )

    await send_email(email, "RACTF - Verify your email", "verify", url=VERIF_EMAIL_BASE.format(uuid))

    return success({})


@app.route("/verify", methods=["POST"])
@limiter("6/15m")
@json_params("uuid")
async def verify(request, uuid):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow(
            "SELECT email_verified, name, hashed_password, id FROM users WHERE id = $1", uuid
        )
        if row is not None:
            if row[0]:
                return abort(400, "Email already verified")

            await conn.execute("UPDATE users SET email_verified = TRUE WHERE id = $1", uuid)
            hm = hmac.new(HMAC_KEY.encode("utf-8"), digestmod=hashlib.sha3_512)
            hm.update(row[1].encode("utf-8"))
            hm.update(row[2].encode("utf-8"))
            hm_token = hm.hexdigest()  # HMAC part of the token
            user_id = str(row[3])  # UUID is returned as an UUID, rather than a string
            token = user_id + "." + hm_token  # Actual token
            return success({"token": token})
        else:
            return abort(400, "Invalid token.")


@app.route("/verify_token", methods=["POST"])
@json_params("token")
async def verify_token(request, token):
    try:
        user_id, token = token.split(".")
        user_id = UUID(user_id)
    except ValueError:
        return abort(403, "Invalid token")

    if await validate_token(token, user_id):
        return success({})
    return abort(403, "Invalid token")


@app.route("/request_password_reset", methods=["POST"])
@limiter("2/20m")
@json_params("username")  # Username can actually be a username or an email
async def request_password_reset(request, username):
    if re.match(r"[^@]+@[^@]+\.[^@]+", username):
        # Strip Gmail + and . addresses
        username = username.strip().lower()
        if username.endswith("@gmail.com"):
            username = username.split("@")[0].split("+")[0].replace(".", "") + "@gmail.com"

    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow(
            "SELECT email, reset_timestamp, id FROM users WHERE name = $1 OR email = $1", username
        )
        if row is None:
            return success({})  # Return success even if user not found to avoid email enum,
            #                     Might be vulnerable to a timing attack
            #                     Is this even a good idea?
        reset_code = pyotp.random_base32(length=32)
        await conn.execute(
            "UPDATE users SET reset_secret = $1, reset_timestamp = $2 WHERE id = $3",
            reset_code,
            datetime.datetime.now(),
            row[2],
        )

        await send_email(
            row[0], "Reset Your Password", "password_reset", url=RESET_EMAIL_BASE.format(row[2], reset_code)
        )
        return success({})


@app.route("/complete_password_reset", methods=["POST"])
@limiter("5/15m")
@json_params("uuid", "secret", "new_password")
async def complete_password_reset(request, uuid, secret, new_password):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        row = await conn.fetchrow("SELECT reset_secret, reset_timestamp FROM users WHERE id = $1", uuid)
        if row is not None:
            if row[1] is None:
                return abort(400, "No password reset requested")
            results = zxcvbn(new_password, user_inputs=["ractf"])
            if results["score"] < 3:
                return abort(400, "Password is too insecure. ")
            if row[0] == secret and (datetime.datetime.now() - row[1]).total_seconds() / 60 < 60:
                await conn.execute(
                    """
                        UPDATE users SET hashed_password = $1, reset_secret = $2 WHERE id = $3
                    """,
                    bcrypt.hashpw(new_password.encode("utf-8"), bcrypt.gensalt()).decode("utf-8"),
                    None,
                    uuid,
                )
                return success({})
            else:
                return abort(403, "Invalid or expired secret specified")
        return abort(400, "Invalid UUID")
