# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import os
import uuid

import pyotp

from starlette.testclient import TestClient

from . import schema
from .routing import app, on_startup

os.environ["RACTF_TEST_ACTIVE"] = "1"

client = TestClient(app)
asyncio.get_event_loop().run_until_complete(on_startup())


async def setup_db():
    testing_data = open("testing_data.sql", "r").read()
    async with app.state.db_pool.acquire() as conn:
        await conn.execute(
            f"""
            DROP TABLE IF EXISTS {",".join(schema.TABLES.keys())} CASCADE;
           """
        )
        schema_sql = schema.prepare()
        for i in schema_sql:
            try:
                await conn.execute(i)
            except:  # noqa: E722, S110
                pass
        await conn.execute(testing_data)


asyncio.get_event_loop().run_until_complete(setup_db())


async def reset_database():
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        await conn.execute("DELETE FROM users CASCADE;")


async def async_get_uuid(username):
    pool = app.state.db_pool
    async with pool.acquire() as conn:
        return await conn.fetchval("SELECT id FROM users WHERE name = $1;", username)


def get_uuid(username):
    return str(asyncio.get_event_loop().run_until_complete(async_get_uuid(username)))


def get_token(user, password):
    resp = client.post("/auth/login", json={"username": user, "password": password})
    return resp.json()["d"]["token"]


def test_reset_database():
    # Just resets the database (somewhat tests database as well)
    asyncio.get_event_loop().run_until_complete(reset_database())


def test_auth_bad_params():
    # Tests if the decorator to unpack params will reject the request if not all are supplied correctly
    resp = client.post("/auth/register", json={})
    assert resp.status_code == 400
    resp = client.post("/auth/register", json={"email": "user@mail.com"})
    assert resp.status_code == 400


def test_auth_register():
    # Test registering
    resp = client.post(
        "/auth/register",
        json={"email": "user@mail.com", "username": "user1", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 200
    assert resp.json()["s"] is True
    # Test disallowed username chars
    resp = client.post(
        "/auth/register",
        json={"email": "user2@mail.com", "username": "an@n", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Username cannot contain @"
    # Test duplicate information disallowed
    resp = client.post(
        "/auth/register",
        json={"email": "user@mail.com", "username": "user2", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Email already in use"
    resp = client.post(
        "/auth/register",
        json={"email": "user2@mail.com", "username": "user1", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Username already in use"
    # Test Gmail Stripping
    resp = client.post(
        "/auth/register",
        json={"email": "user@gmail.com", "username": "user3", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 200
    assert resp.json()["s"] is True
    resp = client.post(
        "/auth/register",
        json={"email": "user+2@gmail.com", "username": "user4", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Email already in use"
    resp = client.post(
        "/auth/register",
        json={"email": "us.er@gmail.com", "username": "user4", "password": "S3cureP@ssW0rd!"},
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Email already in use"


def test_auth_verify():
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!"}
    )
    assert resp.status_code == 403
    assert resp.json()["m"] == "Email unverified"
    # I haven't set it up to check emails yet
    # So we manually pull out their UUID from
    # The database to verify their account.
    uuid = get_uuid("user3")
    print(uuid)
    resp = client.post("/auth/verify", json={"uuid": uuid})
    print(resp.text)
    assert resp.status_code == 200
    assert resp.json()["s"] is True


def test_login():
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!"}
    )
    assert resp.status_code == 200
    assert resp.json()["d"]["token"] is not None
    resp = client.post(
        "/auth/login", json={"username": "nonuser@gmail.com", "password": "S3cureP@ssW0rd!"}
    )
    assert resp.status_code == 403
    assert resp.json()["m"] == "Incorrect username/password"
    resp = client.post("/auth/login", json={"username": "user@gmail.com", "password": "hunter2"})
    assert resp.status_code == 403
    assert resp.json()["m"] == "Incorrect username/password"


def test_validate_token():
    token = get_token("user@gmail.com", "S3cureP@ssW0rd!")
    resp = client.post("/auth/verify_token", json={"token": token})
    assert resp.status_code == 200
    assert resp.json()["s"] is True
    resp = client.post("/auth/verify_token", json={"token": "sdsdsdsd"})
    assert resp.status_code == 403
    assert resp.json()["m"] == "Invalid token"


def test_auth_backend():
    token = get_token("user@gmail.com", "S3cureP@ssW0rd!")
    resp = client.get("/members/self", headers={"Authorization": token})
    assert resp.status_code == 200
    assert resp.json()["d"]["username"] == "user3"
    resp = client.get("/members/self", headers={"Authorization": "dsdsw"})
    assert resp.status_code == 400
    resp = client.get("/members/self")
    assert resp.status_code == 403


def test_team_creation():
    token = get_token("user@gmail.com", "S3cureP@ssW0rd!")
    uuid = get_uuid("user1")
    resp = client.post("/auth/verify", json={"uuid": uuid})
    token2 = get_token("user1", "S3cureP@ssW0rd!")
    resp = client.post(
        "/teams/create", json={"name": "", "password": "pass"}, headers={"Authorization": token}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Name and password must not be empty"
    resp = client.post(
        "/teams/create", json={"name": "name", "password": ""}, headers={"Authorization": token}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Name and password must not be empty"
    resp = client.post(
        "/teams/create", json={"name": "A Team", "password": "pass"}, headers={"Authorization": token}
    )
    print(resp.json())
    assert resp.status_code == 200
    resp = client.post(
        "/teams/create", json={"name": "A Team", "password": "pass"}, headers={"Authorization": token}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "You are already in a team"
    resp = client.post(
        "/teams/create", json={"name": "A Team", "password": "pass"}, headers={"Authorization": token2}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "This team name is already in use"


def test_team_joining():
    uuid = get_uuid("user1")
    resp = client.post("/auth/verify", json={"uuid": uuid})
    token = get_token("user1", "S3cureP@ssW0rd!")
    resp = client.post(
        "/teams/join", json={"name": "B Team", "password": "pass"}, headers={"Authorization": token}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Invalid team name or password"
    resp = client.post(
        "/teams/join", json={"name": "A Team", "password": "password"}, headers={"Authorization": token}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "Invalid team name or password"
    resp = client.post(
        "/teams/join", json={"name": "A Team", "password": "pass"}, headers={"Authorization": token}
    )
    assert resp.status_code == 200
    resp = client.post(
        "/teams/join", json={"name": "A Team", "password": "pass"}, headers={"Authorization": token}
    )
    assert resp.status_code == 400
    assert resp.json()["m"] == "You are already in a team"


def test_get_team_info():
    resp = client.post(
        "/auth/register",
        json={"email": "user@mail2.com", "username": "user4", "password": "S3cureP@ssW0rd!"},
    )
    uuid = get_uuid("user4")
    resp = client.post("/auth/verify", json={"uuid": uuid})
    token_no_team = get_token("user4", "S3cureP@ssW0rd!")
    token = get_token("user@gmail.com", "S3cureP@ssW0rd!")
    resp = client.get("/teams/self", headers={"Authorization": token_no_team})
    assert resp.status_code == 404
    assert resp.json()["m"] == "You are not in a team"
    resp = client.get("/teams/self", headers={"Authorization": token})
    assert resp.status_code == 200
    data = resp.json()["d"]
    print(data)
    assert data["name"] == "A Team"
    assert data["password"] == "pass"
    assert data["owner"]["name"] == "user3"
    assert len(data["members"]) == 2


def test_patch_team_info():
    owner_token = get_token("user3", "S3cureP@ssW0rd!")
    member_token = get_token("user1", "S3cureP@ssW0rd!")
    token_no_team = get_token("user4", "S3cureP@ssW0rd!")
    for field in ["name", "password", "owner"]:
        resp = client.patch("/teams/self", headers={"Authorization": owner_token}, json={field: ""})
        assert resp.status_code == 400
        assert resp.json()["m"] == (f"The {field} field must not" " be empty if present in the request")
    resp = client.patch("/teams/self", headers={"Authorization": token_no_team}, json={"name": "The L Team"})
    assert resp.status_code == 400
    assert resp.json()["m"] == "You are not in a team"
    resp = client.patch("/teams/self", headers={"Authorization": member_token}, json={"name": "The L Team"})
    assert resp.status_code == 403
    assert resp.json()["m"] == "Only the owner is allowed to make changes to the team"
    resp = client.post(
        "/teams/create",
        headers={"Authorization": token_no_team},
        json={"name": "The C Team", "password": "pass"},
    )
    resp = client.patch("/teams/self", headers={"Authorization": owner_token}, json={"name": "The C Team"})
    assert resp.status_code == 400
    assert resp.json()["m"] == "There is an existing team with this name"
    resp = client.patch("/teams/self", headers={"Authorization": owner_token}, json={"owner": "bob"})
    assert resp.status_code == 400
    assert resp.json()["m"] == "Invalid UUID"
    not_present_id = get_uuid("user4")
    resp = client.patch("/teams/self", headers={"Authorization": owner_token}, json={"owner": not_present_id})
    assert resp.status_code == 400
    assert resp.json()["m"] == "The user being promoted to owner is not in the team"

    resp = client.patch(
        "/teams/self", headers={"Authorization": owner_token}, json={"name": "You lost the Game"}
    )
    assert resp.status_code == 200
    team_data = client.get("/teams/self", headers={"Authorization": owner_token})
    assert team_data.json()["d"]["name"] == "You lost the Game"
    resp = client.patch(
        "/teams/self",
        headers={"Authorization": owner_token},
        json={"password": "https://wiki.lesswrong.com/wiki/Roko's_basilisk"},
    )
    assert resp.status_code == 200
    team_data = client.get("/teams/self", headers={"Authorization": owner_token})
    assert team_data.json()["d"]["password"] == ("https://wiki.lesswrong.com/" "wiki/Roko's_basilisk")
    new_owner_id = get_uuid("user1")
    resp = client.patch("/teams/self", headers={"Authorization": owner_token}, json={"owner": new_owner_id})
    assert resp.status_code == 200
    team_data = client.get("/teams/self", headers={"Authorization": owner_token})
    assert team_data.json()["d"]["owner"]["id"] == new_owner_id


def test_team_id_info():
    token = get_token("user1", "S3cureP@ssW0rd!")
    resp = client.get("/teams/definitelynotanid")
    assert resp.status_code == 400
    assert resp.json()["m"] == "Invalid UUID"
    resp = client.get(f"/teams/{str(uuid.uuid4())}")
    assert resp.status_code == 404
    assert resp.json()["m"] == "There is not a team with that information"
    team_id = client.get("/teams/self", headers={"Authorization": token}).json()["d"]["id"]
    resp = client.get(f"/teams/{team_id}")
    team_data = resp.json()["d"]
    assert team_data["name"] == "You lost the Game"
    assert team_data["password"] == "https://wiki.lesswrong.com/wiki/Roko's_basilisk"
    assert team_data["owner"]["name"] == "user1"
    assert len(team_data["members"]) == 2


def test_2fa():
    # Get tokens and stuff
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!"}
    )
    assert resp.status_code == 200
    token = resp.json()["d"]["token"]
    # Check members/self information is correct
    resp = client.get("/members/self", headers={"Authorization": token})
    assert resp.status_code == 200
    assert resp.json()["d"]["2fa_status"] == "off"
    # Add 2fa to the account
    resp = client.post("/auth/add_2fa", headers={"Authorization": token})
    totp_secret = resp.json()["d"]["totp_secret"]
    assert resp.status_code == 200
    assert totp_secret is not None
    # Check user data updated
    resp = client.get("/members/self", headers={"Authorization": token})
    assert resp.status_code == 200
    assert resp.json()["d"]["2fa_status"] == "needs_verify"
    # We should be able to log in without 2fa if it hasn't been verified yet
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!"}
    )
    assert resp.status_code == 200
    assert resp.json()["d"]["token"] is not None
    # Verify our 2fa
    # Make sure we can't verify without a valid code
    resp = client.post("/auth/verify_2fa", json={"otp": "000000"}, headers={"Authorization": token})
    assert resp.status_code == 401
    assert resp.json()["m"] == "Incorrect 2FA token"
    # Actually verify
    totp = pyotp.TOTP(totp_secret)
    resp = client.post("/auth/verify_2fa", json={"otp": totp.now()}, headers={"Authorization": token})
    assert resp.status_code == 200
    # Check we can't login if we don't supply an otp
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!"}
    )
    assert resp.status_code == 401
    assert resp.json()["m"] == "2FA required"
    # Login properly
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!", "otp": totp.now()}
    )
    print(totp.now())
    assert resp.status_code == 200
    assert resp.json()["d"]["token"] is not None
    # Check we can't use an invalid 2fa code
    resp = client.post(
        "/auth/login", json={"username": "user@gmail.com", "password": "S3cureP@ssW0rd!", "otp": "000000"}
    )
    assert resp.status_code == 401
    assert resp.json()["m"] == "Incorrect 2FA token"
    # Check we can regenerate keys
    resp = client.post("/auth/add_2fa", headers={"Authorization": token})
    new_totp_secret = resp.json()["d"]["totp_secret"]
    assert resp.status_code == 200
    assert totp_secret is not None
    assert new_totp_secret != totp_secret
    # TODO: Proper tests with new key


def test_reset_password():
    # TODO: this
    pass
