# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import time
from functools import wraps
from json.decoder import JSONDecodeError

import aiohttp

from .config import config
from .response_util import abort

MAIL_SOCK = config.get("email", "socket")
SEND_MAIL = config.getboolean("email", "send_email")


def _parse_rate_string(rate):
    try:
        num, bucket = rate.split("/")
    except ValueError:
        raise ValueError("Invalid limit format. Expected 'num/bucket'")
    try:
        num = int(num)
    except ValueError:
        raise ValueError(f"'num' must be an integer. Saw '{num}'")

    unit = bucket[-1].lower()
    if unit.isdigit():
        unit = "s"
    else:
        bucket = bucket[:-1]
    if unit not in "hms":
        raise ValueError(f"Invalid bucket unit: '{unit}'")
    else:
        unit = {"h": 60 * 60, "m": 60, "s": 1}[unit]
    try:
        bucket = int(bucket)
    except ValueError:
        raise ValueError(f"'bucket' must be an integer. Saw '{bucket}'")

    return num, bucket * unit


def limiter(rate):
    num, bucket_size = _parse_rate_string(rate)
    buckets = {}

    def decorator(f):
        @wraps(f)
        async def wrapper(request, *args, **kwargs):
            if "RACTF_TEST_ACTIVE" in os.environ:
                return await f(request, *args, **kwargs)

            now = time.time()
            if request.client.host not in buckets:
                buckets[request.client.host] = []

            bucket = buckets[request.client.host]
            while bucket and (now - bucket[0]) > bucket_size:
                bucket.pop(0)
            if len(bucket) >= num:
                return abort(429)

            bucket.append(now)
            return await f(request, *args, **kwargs)

        return wrapper

    return decorator


def json_params(*args, **kwargs):
    def decorator(function):
        @wraps(function)
        async def wrapper(request, *f_args, **f_kwargs):
            try:
                data = await request.json()
            except JSONDecodeError:
                if request.method == "GET":
                    data = {}
                else:
                    return abort(400)

            if any(i not in data and i not in kwargs for i in tuple(args)):
                return abort(400)

            extra = {}
            for key in args:
                extra[key] = data.get(key, kwargs.get(key))

            return await function(request, *f_args, **extra, **f_kwargs)

        return wrapper

    return decorator


async def send_email(send_to, subject_line, template_name, **template_details):
    if SEND_MAIL and not os.environ.get("RACTF_TEST_ACTIVE"):
        conn = aiohttp.UnixConnector(path=MAIL_SOCK)
        async with aiohttp.ClientSession(connector=conn) as session:
            return await session.post(
                "http://unix:/send",
                data={"to": send_to, "subject": subject_line, "template": template_name, **template_details},
            )
    print(
        f"Sending email '{subject_line}' to {send_to} using template {template_name} with details {template_details}"
    )
