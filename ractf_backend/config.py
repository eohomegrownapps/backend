# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import configparser
import os

config = configparser.ConfigParser()

# this isn't in a function because it is only ever going to be run once
filename = os.environ.get("RACTF_CONFIG", "ractf_config.ini")
try:
    with open(filename) as fp:
        config.read_file(fp)
except FileNotFoundError as e:
    raise ValueError(
        "Couldn't find config file {0} - consider setting env-var RACTF_CONFIG to point to the config file".format(
            filename
        )
    ) from e

# use it with `from .config import config`
