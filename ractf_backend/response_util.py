# RACTF Backend
# Copyright (C) 2019  RACTF Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import http

from starlette.responses import JSONResponse


def abort(status, message=None):
    if message is None:
        try:
            message = http.HTTPStatus(status).phrase
        except ValueError:
            message = ""

    return JSONResponse({"s": False, "m": message}, status_code=status)


def success(payload, **kwargs):
    return JSONResponse({"s": True, "d": payload}, **kwargs)
