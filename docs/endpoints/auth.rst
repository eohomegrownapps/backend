===============================================================================
                            Authentication Endpoints
===============================================================================

**URL Base:** :code:`/auth`


******************************************************************************
                                 POST /add_2fa
******************************************************************************

.. note::

    Authentication Required

Add TOTP based 2fa to an account and return the TOTP secret.
This request does not require any additional parameters.

Success data:
The returned data object will have the following fields:

+----------------------+---------+-------------------------------------------+
| Field                | Type    | Description                               |
+----------------------+---------+-------------------------------------------+
| totp_secret          | string  | The TOTP Secret that should be used       |
|                      |         | to generate TOTP codes for                |
|                      |         | :code:`/auth/login`                       |
+----------------------+---------+-------------------------------------------+

.. note::

    POSTing this endpoint when 2fa is already enabled on an account will
    generate a new 2fa token and invalidate the old one. It will change the
    user's 2fa status to :code:`needs_verify`, and they will be asked to 
    verify their 2fa before being able to use it to login.
    
.. note::

    After POSTing this endpoint, a user should verify their 2fa via
    :code:`/auth/verify_2fa`. They will not be asked to use the 2-factor
    code during login before verifying.


******************************************************************************
                                POST /verify_2fa
******************************************************************************

.. note::

    Authentication Required

Verify that a user has correctly setup TOTP on their account

+-----------+---------+--------------------------------------------+---------+
| Field     | Type    | Description                                | Default |
+-----------+---------+--------------------------------------------+---------+
| otp       | string  | The 2FA token the user generated           | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+

This endpoint is not used to authenticate with 2fa, just to check a 2fa code
is valid for the purpose of validating that 2fa is correctly setup.

.. note::

    A user
    will not be able to login using 2fa if they have not verified via this
    endpoint.

Error Responses:

+------+----------------------------+----------------------------------------+
| Code | Content                    | Reason                                 |
+------+----------------------------+----------------------------------------+
| 400  | 2FA not set up or already  |                                        |
|      | verified                   |                                        |
+------+----------------------------+----------------------------------------+
| 401  | Incorrect 2FA token        | A 2fa token was supplied and was       |
|      |                            | incorrect                              |
+------+----------------------------+----------------------------------------+

Success data:
This endpoint does not return any additional data.


******************************************************************************
                                  POST /login
******************************************************************************

+-----------+---------+--------------------------------------------+---------+
| Field     | Type    | Description                                | Default |
+-----------+---------+--------------------------------------------+---------+
| username  | string  | The user's username                        | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+
| password  | string  | The user's password                        | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+
| otp       | string  | The 2FA token if required                  | None    |
+-----------+---------+--------------------------------------------+---------+

This endpoint is used to generate a session token, the username and password
are first posted to this endpoint and if 2fa is required, the API will return
an error (see below), the client then must resend the username and password
along with a 2fa token.

Error responses:

+------+----------------------------+----------------------------------------+
| Code | Content                    | Reason                                 |
+------+----------------------------+----------------------------------------+
| 403  | Email unverified           | The user has not verified their email  |
+------+----------------------------+----------------------------------------+
| 401  | Incorrect 2FA token        | A 2fa token was supplied and was       |
|      |                            | incorrect                              |
+------+----------------------------+----------------------------------------+
| 401  | 2FA required               | The form must be resubmitted with a    |
|      |                            | 2fa token                              |
+------+----------------------------+----------------------------------------+
| 403  | Incorrect                  | The username or password is incorrect  |
|      | username/password          |                                        |
+------+----------------------------+----------------------------------------+

Success data:
The returned data object will have the following fields:

+----------------------+---------+-------------------------------------------+
| Field                | Type    | Description                               |
+----------------------+---------+-------------------------------------------+
| token                | string  | The token that should be used to          |
|                      |         | authenticate with further requests.       |
|                      |         | (see Authentication)                      |
+----------------------+---------+-------------------------------------------+


******************************************************************************
                                 POST /register
******************************************************************************
Register a user.

+-----------+---------+--------------------------------------------+---------+
| Field     | Type    | Description                                | Default |
+-----------+---------+--------------------------------------------+---------+
| email     | email   | The user's email                           | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+
| username  | string  | The user's username                        | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+
| password  | string  | The user's password                        | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+

This creates a new user.
Error responses:

+------+----------------------------+----------------------------------------+
| Code | Content                    | Reason                                 |
+------+----------------------------+----------------------------------------+
| 400  | Username too long          |                                        |
+------+----------------------------+----------------------------------------+
| 400  | Username too short         |                                        |
+------+----------------------------+----------------------------------------+
| 400  | Username cannot contain @  |                                        |
+------+----------------------------+----------------------------------------+
| 400  | Invalid email address      | email doesn't match                    |
|      |                            | :code:`/[-@]+@[-@]+\.[-@]+/`           |
+------+----------------------------+----------------------------------------+
| 400  | Username already in use    |                                        |
+------+----------------------------+----------------------------------------+

Success data:
This endpoint does not return any additional data.


******************************************************************************
                                  POST /verify
******************************************************************************
Verifies an email address, a user will not be able to login before using this
endpoint.

+-----------+---------+--------------------------------------------+---------+
| Field     | Type    | Description                                | Default |
+-----------+---------+--------------------------------------------+---------+
| uuid      | uuid    | The user's uuid                            | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+

Error Responses:

+------+----------------------------+----------------------------------------+
| Code | Content                    | Reason                                 |
+------+----------------------------+----------------------------------------+
| 400  | Email already verified     |                                        |
+------+----------------------------+----------------------------------------+
| 400  | Invalid token              | No user exists with this UUID          |
+------+----------------------------+----------------------------------------+

Success data:
The returned data object will have the following fields:

+----------------------+---------+-------------------------------------------+
| Field                | Type    | Description                               |
+----------------------+---------+-------------------------------------------+
| token                | string  | The token that should be used to          |
|                      |         | authenticate with further requests.       |
|                      |         | (see Authentication)                      |
+----------------------+---------+-------------------------------------------+


******************************************************************************
                               POST /verify_token
******************************************************************************

Checks whether a token is valid.

+-----------+---------+--------------------------------------------+---------+
| Field     | Type    | Description                                | Default |
+-----------+---------+--------------------------------------------+---------+
| token     | string  | The user's session token                   | \-\-\-  |
+-----------+---------+--------------------------------------------+---------+

Validates a user's session token.
Error responses:

+------+----------------------------+----------------------------------------+
| Code | Content                    | Reason                                 |
+------+----------------------------+----------------------------------------+
| 400  | Invalid Token              |                                        |
+------+----------------------------+----------------------------------------+

Success data:
This endpoint does not return any additional data.


******************************************************************************
                         POST /request_password_reset
******************************************************************************

Sends an email to the requested username's registered email detailing how to reset their password.

+-----------+---------+-----------------------------------------------+---------+
| Field     | Type    | Description                                   | Default |
+-----------+---------+-----------------------------------------------+---------+
| username  | string  | Username referring to the user who wishes     | \-\-\-  |
|           |         | to reset their password. While the field      |         |
|           |         | name is username, it can also be their email. |         |
+-----------+---------+-----------------------------------------------+---------+


Error responses:

This endpoint has no special error responses to avoid email enumeration.

Success data:
This endpoint does not return any additional data.

******************************************************************************
                         POST /complete_password_reset
******************************************************************************

Takes in an emailed code and the user's UUID to complete a password reset.

+--------------+---------+-----------------------------------------------+---------+
| Field        | Type    | Description                                   | Default |
+--------------+---------+-----------------------------------------------+---------+
| uuid         | string  |                                               | \-\-\-  |
+--------------+---------+-----------------------------------------------+---------+
| secret       | string  | A one-time code emailed to the user to        | \-\-\-  |
|              |         | use during password resets. Expires after     |         |
|              |         | either use or 60 minutes.                     |         |
+--------------+---------+-----------------------------------------------+---------+
| new_password | string  | The new password that the user wishes to      | \-\-\-  |
|              |         | set on their RACTF account.                   |         |
+--------------+---------+-----------------------------------------------+---------+


Error responses:

+------+----------------------------+----------------------------------------+
| Code | Content                    | Reason                                 |
+------+----------------------------+----------------------------------------+
| 400  | No password reset requested| The user did not request a reset       |
+------+----------------------------+----------------------------------------+
| 400  | Password too short         |                                        |
+------+----------------------------+----------------------------------------+
| 400  | Invalid UUID               |                                        |
+------+----------------------------+----------------------------------------+
| 403  | Invalid secret             |                                        |
+------+----------------------------+----------------------------------------+



Success data:
This endpoint does not return any additional data.