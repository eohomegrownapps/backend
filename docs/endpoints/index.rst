==================
Endpoint Reference
==================

    This is a reference of all the API endpoints. Enjoy!

..  toctree::
    :maxdepth: 2

    auth
    countdown
    challenges
    members
    teams
