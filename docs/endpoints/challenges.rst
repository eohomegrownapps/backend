===============================================================================
                              Challenges Endpoints
===============================================================================

**URL Base:** :code:`/challenges`


******************************************************************************
                                 GET /
******************************************************************************

Lists all challenges that we know about.

This isn't finished at all yet. It provides too much information (eg, flags!)
for challenges that are still locked (in fact, it doesn't even check if
they're locked, because we don't have plugins finished yet), and it
doesn't provide any info like "has this challenge been solved by the current
user yet" either. That will come soon.

.. note ::
    This endpoint's format is certainly not yet finalised, so I won't document
    it here. It should be reasonably obvious just from looking at it though.

******************************************************************************
                                 GET /<challenge_id>
******************************************************************************

Provide information about a specific challenge.

This will probably provide a little more info than the "list all" endpoint.
(For example, hints). What exactly the difference between them, in terms of
what information is provided, is yet to be fully decided upon.

.. note ::
    This endpoint's format is certainly not yet finalised, so I won't document
    it here. It should be reasonably obvious just from looking at it though.




******************************************************************************
                                 POST /<challenge_id>/attempt
******************************************************************************

Submit a flag attempt for the given challenge.

Request data:

+-----------+---------+--------------------------------------------+---------+
| Field     | Type    | Description                                | Default |
+-----------+---------+--------------------------------------------+---------+
| flag      | string  | The flag submission that is being attempted| \-\-\-  |
+-----------+---------+--------------------------------------------+---------+

Response data:
The returned data object will have the following fields:

+----------------------+---------+-------------------------------------------+
| Field                | Type    | Description                               |
+----------------------+---------+-------------------------------------------+
| correct              | boolean | whether the submitted flag was correct    |
+----------------------+---------+-------------------------------------------+
