------------------
Plugins documentation
------------------

This page will probably be vastly improved in the near-ish future as the
plugin system itself but it's probably a good idea to write up what I have
so far.

************
bases.py
************
This file contains base classes for plugin types. each individual plugin is
a class, and that class should inherit from a base class in this file,
depending on what type of plugin it is.

************
plugins.py
************
This is the external plugin interface, for other parts of the application
that want to use plugins to defer some overrideable functionality. it is used
as follows. It provides the global object ``plugins``, which is used as
follows::

    from ractf_backend.plugins import plugins # import the global obj

    # say we want the challenge-type-plugin for the challenge described
    # by record. we can do this like this: (constructing a new instance)

    plugin = plugins.challenge_type[record.type](record)

    # or, equivalently, like this:

    plugin = plugins.challenge_type(record) # automatically works out the type
