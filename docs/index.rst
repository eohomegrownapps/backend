=========
RACTF API
=========

-------------------
General Information
-------------------
This backend is the backend that powers Really Awesome CTF. This is the
documentation for the API.

.. toctree::
    :caption: User Guide
    :maxdepth: 2

    topics
    plugins
    endpoints/index.rst
