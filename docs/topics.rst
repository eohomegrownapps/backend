------------------
Response structure
------------------
All responses will come as a JSON object with a boolean :code:`s` field
indicating success. A request with a :code:`s` value of :code:`False` may
contain a message under the :code:`m` parameter. An successful request may
contain additional data, as a nested JSON object, under the :code:`d` field.

--------------
Authentication
--------------
Authentication should be passed to any route that requires it in the form of
an Authorization header. The value should be a token retrieved from
:code:`/auth/login`, in the form :code:`Authorization: token`.

.. note::

    There is no preceeding :code:`Bearer`. This is intentional.
